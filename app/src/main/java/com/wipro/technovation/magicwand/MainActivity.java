package com.wipro.technovation.magicwand;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Random;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity {

    //node Socket
    //Node socket.io variables
    private Socket mSocket;
    JSONObject data = new JSONObject();

    //UI Elements
    private ProgressDialog progress;
    private EditText etMessage;
    private ImageButton ibSend;
    private TextView tvLog;

    //String variables
    String yourInput = "You: ";
    String neoReality = "Neo Reality: ";
    String lineBreak = "**************************************";
    String errorMessages[] = {"err,I don't know what to do now",
            "Bad command or file name :P",
            "Seriously! Confusing me will not achieve anything",
            "sorry dunno what to do with that command",
            "ever considered reading the manual",
            "God help me with this one, doesn't seem to know the commands",
            "please check your command and try again...please",
            "I am going to jail for executing that command.. seems to be.. wrong!",
            "uh uh.. here comes the testing folks...command no good",
            "negative..are we on the same page here?",
            "developer had only limited commands in his mind!, "
    };

    String goodMessages[] = {
            "Aye! Aye! Captain...",
            "Done Sir!",
            "You should see the desired output now",
            "Gotcha!",
            "You betcha!",
            "Done boss!",
            "All set!",
            "You should get your output.. now!",
            "Alrighty!",
            "Yo! its done",
            "With me there can't be a miss",
            "Yes sir!",
            "Affirmative",
            "sir!! yes sir!!"
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etMessage = (EditText) findViewById(R.id.etMessage);
        ibSend = (ImageButton) findViewById(R.id.ibSend);
        tvLog = (TextView) findViewById(R.id.tvLog);
        tvLog.setMovementMethod(new ScrollingMovementMethod());
        tvLog.setTextColor(Color.parseColor("#42a1f4"));

        progress = new ProgressDialog(MainActivity.this);
        progress.setTitle("Loading");
        progress.setMessage("connecting to Neo Reality...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.setIndeterminate(true);
        progress.setProgress(0);

        //UI INIT
        etMessage.setEnabled(false);
        ibSend.setEnabled(false);

        etMessage.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    //Toast.makeText(MainActivity.this, etMessage.getText(), Toast.LENGTH_SHORT).show();
                    sendMessage();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isInternetAvailable()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("Are you online?");
            builder.setMessage("Connect to Internet & try again").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } else {

            try {
                IO.Options opts = new IO.Options();
                opts.forceNew = false;
                opts.reconnection = true;

                mSocket = IO.socket(getString(R.string.wand_server_ip), opts);
                mSocket.on(Socket.EVENT_CONNECT, onSocketConnected);
                mSocket.on(Socket.EVENT_DISCONNECT, onSocketDisconnected);
                mSocket.on(Socket.EVENT_CONNECTING, onSocketConnecting);
                mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onSocketTimeOut);
                mSocket.connect();

            } catch (URISyntaxException e) {
                Log.d(getString(R.string.debug_tag), "mySocket connection error: " + e.toString());
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSocket.connected()) {
            mSocket.disconnect();
            mSocket.off(Socket.EVENT_CONNECT, onSocketConnected);
            mSocket.off(Socket.EVENT_DISCONNECT, onSocketDisconnected);
            mSocket.off(Socket.EVENT_CONNECTING, onSocketConnecting);
            mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onSocketTimeOut);
        }
    }

    public void onSendMessage(View v) {
        sendMessage();

    }

    private void sendMessage() {
        String message = etMessage.getText().toString();
        if (message.length() > 0) {
            etMessage.setText("");
            analyzeMessage(message.toLowerCase());
            updateLog(yourInput + message);
            updateLog(lineBreak);
        }
    }


    private void analyzeMessage(String message) {
        Log.d(getString(R.string.debug_tag), message);
        try {
            //controls for LED 1
            if (message.contains("light1") || message.contains("light 1") ||
                    message.contains("led1") || message.contains("led 1")) {
                if (message.contains("on") || message.contains("up")) {
                    data.put("opr", "1");
                } else if (message.contains("off") || message.contains("down")) {
                    data.put("opr", "2");
                } else if (message.contains("blink")) {
                    data.put("opr", "3");
                }
            } else if (message.contains("light2") || message.contains("light 2") ||
                    message.contains("led2") || message.contains("led 2")) {
                if (message.contains("on") || message.contains("up")) {
                    data.put("opr", "4");
                } else if (message.contains("off") || message.contains("down")) {
                    data.put("opr", "5");
                } else if (message.contains("blink")) {
                    data.put("opr", "6");
                }
            } else if (message.contains("buzz")) {
                if (message.contains("on") || message.contains("up")) {
                    data.put("opr", "7");
                } else if (message.contains("off") || message.contains("down")) {
                    data.put("opr", "8");
                } else if (message.contains("blink") || message.contains("beep")) {
                    data.put("opr", "9");
                }
            } else if (message.contains("motor") || message.contains("fan")) {
                if (message.contains("on") || message.contains("up")) {
                    data.put("opr", "10");
                } else if (message.contains("off") || message.contains("down")) {
                    data.put("opr", "11");
                }
            } else if (message.contains("repeat") || message.contains("again")) {
                data.put("opr", data.getString("back"));
            } else {
                data.remove("opr");
            }


            if (data.has("opr")) {
                data.put("back", data.getString("opr"));
                mSocket.emit("hack", data);
                Random r = new Random();
                int index = r.nextInt(goodMessages.length - 1) + 0;
                Log.d(getString(R.string.debug_tag), "index: " + index + "msg: " + goodMessages[index]);
                //tvLog.setTextColor(Color.parseColor("#42f480"));
                updateLog(neoReality + goodMessages[index]);
            } else {
                Random r = new Random();
                int index = r.nextInt(errorMessages.length - 1) + 0;
                Log.d(getString(R.string.debug_tag), "index: " + index + "msg: " + errorMessages[index]);
                //tvLog.setTextColor(Color.parseColor("#f4a142"));
                updateLog(neoReality + errorMessages[index]);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Emitter.Listener onSocketConnected = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            Log.d(getString(R.string.debug_tag), "onSocketConnected");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progress.dismiss();
                    etMessage.setEnabled(true);
                    ibSend.setEnabled(true);
                    Toast.makeText(MainActivity.this, "Connection Successful!!", Toast.LENGTH_SHORT).show();
                }
            });

        }
    };

    private Emitter.Listener onSocketDisconnected = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            Log.d(getString(R.string.debug_tag), "onSocketDisConnected");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    etMessage.setEnabled(false);
                    ibSend.setEnabled(false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setCancelable(false);
                    builder.setTitle("We lost you..");
                    builder.setMessage("We lost connectivity.. please try again!").setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                }
            });
        }
    };

    private Emitter.Listener onSocketConnecting = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            Log.d(getString(R.string.debug_tag), "onSocketconnecting...");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progress.show();
                    etMessage.setEnabled(false);
                    ibSend.setEnabled(false);

                }
            });
        }
    };

    private Emitter.Listener onSocketTimeOut = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            Log.d(getString(R.string.debug_tag), "onSocketTime Out");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progress.show();
                    etMessage.setEnabled(false);
                    ibSend.setEnabled(false);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setCancelable(false);
                    builder.setTitle("Oops..");
                    builder.setMessage("Could'nt connect to Neo Reality... Try Later!!").setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                }
            });

        }
    };

    public boolean isInternetAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return (cm.getActiveNetworkInfo() != null);
    }

    private void updateLog(String s) {
        String history = tvLog.getText().toString();
        tvLog.setText(s + "\n" + history);
    }

    public void clearLog(View v) {
        tvLog.setText("");
    }
}
